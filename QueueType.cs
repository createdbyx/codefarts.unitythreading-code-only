// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueueType.cs" company="Codefarts">
//   contact@codefarts.com
//   http://www.codefarts.com
//   ----------------------------
//   This code is a heavily modified version of the Loom threading class.
//   Source webpage: http://unitygems.com/threads/
//   Author: whydoidoit
//   Original license (stated by author): MIT licensed, use it as you see fit!
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Codefarts.UnityThreading
{
    /// <summary>
    ///     Defines values used to determine when a callback is to be called.
    /// </summary>
    public enum QueueType
    {
        /// <summary>
        ///     Callback will be executed when Update is called by unity.
        /// </summary>
        Update, 

        /// <summary>
        ///     Callback will be executed when OnPostRender is called by unity.
        /// </summary>
        OnPostRender, 

        /// <summary>
        ///     Callback will be executed when FixedUpdate is called by unity.
        /// </summary>
        FixedUpdate, 

        /// <summary>
        ///     Callback will be executed when OnPreRender is called by unity.
        /// </summary>
        OnPreRender, 

        /// <summary>
        ///     Callback will be executed on a new background thread.
        /// </summary>
        BackgroundThread
    }
}