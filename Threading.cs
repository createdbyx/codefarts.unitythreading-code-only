// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Threading.cs" company="Codefarts">
//   contact@codefarts.com
//   http://www.codefarts.com
//   ----------------------------
//   This code is a heavily modified version of the Loom threading class.
//   Source webpage: http://unitygems.com/threads/
//   Author: whydoidoit
//   Original license (stated by author): MIT licensed, use it as you see fit!
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Codefarts.UnityThreading
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    using UnityEngine;

    /// <summary>
    ///     Provides a class for multi-threading in unity.
    /// </summary>
    public class Threading : MonoBehaviour
    {
        #region Static Fields

        /// <summary>
        ///     Holds a reference to the game object that the <see cref="Threading" /> behaviour is attached to.
        /// </summary>
        private static GameObject gameObjectReference;

        /// <summary>
        ///     Holds a singleton reference to the <see cref="Threading" /> class.
        /// </summary>
        //  private static Threading singleton;

        #endregion

        #region Fields

        /// <summary>
        ///     A dictionary used to hold the current executing task list.
        /// </summary>
        /// <remarks>This is used to prevent having to create a temporary list every time we ned to execute a lists of tasks.</remarks>
        private static List<Task> currentlyExecutingTasks = new List<Task>();

        /// <summary>
        ///     A dictionary containing the queued lsit fo tasks to be executed.
        /// </summary>
        private static Dictionary<QueueType, List<Task>> queuedTasks;

        /// <summary>
        ///     Holds the maximum number of threads that can be queued.
        /// </summary>
        /// <remarks>The default value is 8.</remarks>
        private static int maxThreads = 8;

        /// <summary>
        ///     Used to record the number of currently running threads.
        /// </summary>
        private static int numThreads;

        private static bool isInitilized;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Threading" /> class.
        /// </summary>
        static Threading()
        {
            // setup the dictionary lists so that they are ready to go
            var queueTypes = Enum.GetValues(typeof(QueueType)) as QueueType[];
            currentlyExecutingTasks = new List<Task>();
            queuedTasks = new Dictionary<QueueType, List<Task>>();
            foreach (QueueType value in queueTypes)
            {
                queuedTasks[value] = new List<Task>();
            }
        }

        #endregion

        #region Public Properties

        ///// <summary>
        /////     Gets the singleton instance.
        ///// </summary>
        //public static Threading Instance
        //{
        //    get
        //    {
        //        if (singleton == null)
        //        {
        //            gameObjectReference = GameObject.Find("Codefarts.UnityThreading Singleton");
        //            if (gameObjectReference == null)
        //            {
        //                gameObjectReference = new GameObject("Codefarts.UnityThreading Singleton");
        //            }

        //            singleton = gameObjectReference.AddComponent<Threading>();
        //        }

        //        return singleton;
        //    }
        //}

        /// <summary>
        /// Gets or sets the maximum number of threads.
        /// </summary>
        /// <exception cref="System.ArgumentOutOfRangeException">Will be thrown if set to a value Less then 1.</exception>
        public static int MaxThreads
        {
            get
            {
                return maxThreads;
            }

            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                maxThreads = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Queues a task for immediate execution on the Update event.
        /// </summary>
        /// <param name="task">
        /// The callback to be executed.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// If the 'task' parameter is null.
        /// </exception>
        /// <returns>Returns a reference to the task that was queued.</returns>
        public static Task QueueTask(Action<Task> task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            return QueueTask(new Task { action = task, Type = QueueType.Update });
        }

        /// <summary>
        /// Queues a task for delayed execution on the Update event.
        /// </summary>
        /// <param name="task">
        /// The callback to be executed.
        /// </param>
        /// <param name="time">
        /// The time to wait before the task is executed.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// If the 'task' parameter is null.
        /// </exception>
        /// <returns>Returns a reference to the task that was queued.</returns>
        public static Task QueueTask(Action<Task> task, float time)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            return QueueTask(new Task { action = task, Type = QueueType.Update, time = time });
        }

        /// <summary>
        /// Queues a task for delayed execution on the Update event.
        /// </summary>
        /// <param name="task">
        /// The callback to be executed.
        /// </param>
        /// <param name="time">
        /// The time to wait before the task is executed.
        /// </param>
        /// <param name="type">Controls when the task will be run.</param>
        /// <exception cref="System.ArgumentNullException">
        /// If the 'task' parameter is null.
        /// </exception>
        /// <returns>Returns a reference to the task that was queued.</returns>
        public static Task QueueTask(Action<Task> task, float time, QueueType type)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            return QueueTask(new Task { action = task, Type = type, time = time });
        }

        /// <summary>
        /// Queues a task for delayed execution on the Update event.
        /// </summary>
        /// <param name="task">
        /// The callback to be executed.
        /// </param>
        /// <param name="type">Controls when the task will be run.</param>
        /// <exception cref="System.ArgumentNullException">
        /// If the 'task' parameter is null.
        /// </exception>
        /// <returns>Returns a reference to the task that was queued.</returns>
        public static Task QueueTask(Action<Task> task, QueueType type)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            return QueueTask(new Task { action = task, Type = type });
        }

        /// <summary>
        /// Queues a series of tasks for execution.
        /// </summary>
        /// <param name="tasks">
        /// The tasks to be executed.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// If the 'task' parameter is null.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// The task parameter has a continuation with no callback assigned!
        /// </exception>
        public static void QueueTask(IEnumerable<Task> tasks)
        {
            foreach (var task in tasks)
            {
                QueueTask(task);
            }
        }

        /// <summary>
        /// Queues a task for delayed execution.
        /// </summary>
        /// <param name="task">
        /// The task to be executed.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// If the 'task' parameter is null.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// The task parameter has a continuation with no callback assigned!
        /// </exception>
        /// <returns>Returns a reference to the task that was queued.</returns>
        public static Task QueueTask(Task task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            if (task.continueWith != null && task.continueWith.action == null)
            {
                throw new ArgumentException("The task parameter has a continuation with no callback assigned!", "task");
            }

            if (task.IsRunning)
            {
                throw new ArgumentException("The task is currently running.", "task");
            }

            if (task.IsCompleted)
            {
                throw new ArgumentException("The task has already completed.", "task");
            }

            if (task.IsCanceled)
            {
                throw new ArgumentException("The task was previously canceled.", "task");
            }

            Initialize();

            if (task.Type == QueueType.BackgroundThread)
            {
                // If we have reached the maximum number of threads wait until a thread is available
                while (numThreads >= maxThreads)
                {
                    Thread.Sleep(1);
                }

                Interlocked.Increment(ref numThreads);
                ThreadPool.QueueUserWorkItem(RunAction, task);
            }
            else
            {
                lock (queuedTasks)
                {
                    queuedTasks[task.Type].Add(task);
                }
            }     

            return task;
        }

        /// <summary>
        /// Initializes the game object responsible for executing the various queued tasks.
        /// </summary>
        private static void Initialize()
        {
            if (isInitilized)
            {
                return;
            }

            gameObjectReference = GameObject.Find("Codefarts.UnityThreading");
            if (gameObjectReference == null)
            {
                gameObjectReference = new GameObject("Codefarts.UnityThreading");
            }

            gameObjectReference.AddComponent<Threading>();
            isInitilized = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes queued callbacks for a given queue type.
        /// </summary>
        /// <param name="type">
        /// The type of callbacks to execute.
        /// </param>
        private void DoCallbacks(QueueType type)
        {
            // temporarily lock the queued tasks dictionary
            lock (queuedTasks)
            {
                // clear the currently executing task list
                currentlyExecutingTasks.Clear();
                var taskArray = new Task[1000];
                int count = 0;

                // process each queued task and add it to the tasks array
                int i = 0;
                var taskList = queuedTasks[type];
                while (i < taskList.Count)
                {
                    Task task = taskList[i];

                    // check if it's time to execute
                    if (task.time <= Time.time)
                    {
                        taskArray[count] = task;
                        count++;

                        // check if we have filled up the taskArray and if so allocate more room for more tasks
                        if (count >= taskArray.Length)
                        {
                            Array.Resize(ref taskArray, (int)(taskArray.Length * 1.25f));
                        }

                        // we have added the task to the task array so we no longer need it in the task list
                        taskList.RemoveAt(i);
                        continue;
                    }

                    i++;
                }

                // if we have added tasks add then to the currently executing tasks list
                if (count > 0)
                {
                    if (taskArray.Length > count)
                    {
                        Array.Resize(ref taskArray, count);
                    }

                    // clear the currently executing task list and add the new tasks
                    currentlyExecutingTasks.Clear();
                    currentlyExecutingTasks.AddRange(taskArray);
                }
            }

            foreach (Task task in currentlyExecutingTasks)
            {
                 
                    task.Run();

                    // check if the task has a continuation and if so queue it up
                    if (!task.IsCanceled && task.continueWith != null)
                    {
                        QueueTask(task.continueWith);
                    }
                
            }
        }

        /// <summary>
        ///     Called by unity at fixed time intervals.
        /// </summary>
        private void FixedUpdate()
        {
            this.DoCallbacks(QueueType.FixedUpdate);
        }

        /// <summary>
        ///     Called by unity after rendering has completed.
        /// </summary>
        private void OnPostRender()
        {
            this.DoCallbacks(QueueType.OnPostRender);
        }

        /// <summary>
        ///     Called by unity before rendering begins.
        /// </summary>
        private void OnPreRender()
        {
            this.DoCallbacks(QueueType.OnPostRender);
        }

        /// <summary>
        /// This method is called by ThreadPool.QueueUserWorkItem and is used to ensure that <see cref="numThreads"/> is
        ///     decremented.
        /// </summary>
        /// <param name="action">
        /// Should contain a reference to the task to be executed.
        /// </param>
        private static void RunAction(object action)
        {
            Task task = null;
            try
            {
                task = (Task)action;
                task.Run();

                //// if it's a background thread task be sure to wait until is has completed
                //if (task.Type == QueueType.BackgroundThread)
                //{
                //    task.Wait();
                //}

                // check if the task has a continuation and if so queue it up
                if (!task.IsCanceled && task.continueWith != null)
                {
                    QueueTask(task.continueWith);
                }
            }
            catch  
            {
            }
            finally
            {
                Interlocked.Decrement(ref numThreads);
            }
        }

        /// <summary>
        ///     Called by unity during every update.
        /// </summary>
        private void Update()
        {
            this.DoCallbacks(QueueType.Update);
        }

        #endregion
    }
}