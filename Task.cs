// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Task.cs" company="Codefarts">
//   contact@codefarts.com
//   http://www.codefarts.com
//   ----------------------------
//   This code is a heavily modified version of the Loom threading class.
//   Source webpage: http://unitygems.com/threads/
//   Author: whydoidoit
//   Original license (stated by author): MIT licensed, use it as you see fit!
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Codefarts.UnityThreading
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading;

    using UnityEngine;

    /// <summary>
    ///     Defines a worker task.
    /// </summary>
    public class Task
    {
        #region Fields

        /// <summary>
        ///     Holds the queue type for this task.
        /// </summary>
        public QueueType Type;

        /// <summary>
        ///     The callback to execute for this task.
        /// </summary>
        public Action<Task> action;

        /// <summary>
        ///     The continuation callback to execute after this task has completed.
        /// </summary>
        public Task continueWith;

        /// <summary>
        ///     Used to determine how long to wait before executing this task.
        /// </summary>
        public float time;

        /// <summary>
        /// Gets the Exception that caused the Task to end prematurely. If the Task completed successfully or has not yet thrown any exceptions, this will return null.
        /// </summary>
        public Exception Exception
        {
            get
            {
                return this.exception;
            }

           private set
            {
                this.exception = value;
            }
        }

        /// <summary>
        /// Holds weather or not this task has been canceled.
        /// </summary>
        private bool isCanceled;

        /// <summary>
        /// Gets a value indicating whether this instance is pending cancel.
        /// </summary>
        public bool IsCanceled
        {
            get
            {
                return this.isCanceled;
            }
        }

        /// <summary>
        /// Gets whether the Task completed due to an unhandled exception.
        /// </summary>
        public bool IsFaulted
        {
            get
            {
                return this.exception!=null;
            }
        }

        /// <summary>
        /// Holds weather or not this task has been completed.
        /// </summary>
        private bool isCompleted;

        /// <summary>
        /// Gets whether this Task has completed.
        /// </summary>
        public bool IsCompleted
        {
            get
            {
                return this.isCompleted;
            }

            private set
            {
                this.isCompleted = value;
                if (value)
                {
                    this.isRunning = false;
                }
            }
        }

        /// <summary>
        /// Holds weather or not this task is currently running.
        /// </summary>
        private bool isRunning;

        private bool isWaiting;

        private Exception exception;

        /// <summary>
        /// Gets whether this Task is currently running.
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return this.isRunning;
            }

            private set
            {
                this.isRunning = value;
            }
        }

        public void Cancel()
        {
            this.isCanceled = true;
        }

        #endregion

        public void Wait()
        {
            while (!this.isCompleted && !this.isCanceled && this.Exception == null)
            {
                Thread.Sleep(1);
            }

            return;
            var scheduler = CoroutineManager.Instance;
            scheduler.StartCoroutine(this.WaitForCompletion());
                   Debug.Log("started wait corrotine");
            //if (this.Type != QueueType.BackgroundThread)
            // {
            //    throw new ThreadStateException("Can only wait on background thread tasks.");
            // }

            //var completed = false;
            //Threading.QueueTask(
            //    t =>
            //        {
            //            while (!this.isCompleted && !this.isCanceled && this.Exception == null)
            //            {
            //                Thread.Sleep(1);
            //            }

            //            completed = true;
            //        },
            //    QueueType.BackgroundThread);

            while (this.isWaiting)
            {
                Debug.Log("  isWaiting loop");
            }
        }

        private IEnumerator WaitForCompletion()
        {
            while (!this.isCompleted && !this.isCanceled && this.Exception == null)
            {
                Debug.Log("waiting");
                this.isWaiting = true;
                yield return new WaitForEndOfFrame();
            }

            Debug.Log("done waiting");
            this.isWaiting = false;
        }

        public void Run()
        {
            this.IsRunning = true;
            try
            {
                this.action(this);
            }
            catch (Exception ex)
            {
                this.Exception = ex;
            }
            finally
            {
                this.IsCompleted = true;
            }
        }
    }
}